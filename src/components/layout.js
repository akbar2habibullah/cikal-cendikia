/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { Container } from 'reactstrap'

import 'bootstrap/dist/css/bootstrap.min.css';

import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {


  return (
    <>
      <div className="head">
        <Header/>
      </div>
      <Container>
        <div className="cover-container d-flex w-100 h-100 mx-auto flex-column">{ children }</div>
        <footer>
          Copyright &copy; 2020 Yayasan Cendikia Salsabila. All Rights Reserved
        </footer>
      </Container>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
