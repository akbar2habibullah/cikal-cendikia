import { Link, useStaticQuery, graphql } from "gatsby"
import React from "react"
import Img from "gatsby-image"

const Header = () => {
  const data = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "cikal.png" }) {
        childImageSharp {
          fixed(width: 50, height: 50) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `) 

  return (
    <header>
      <Link className="nav-link" to="/" ><h3 className="header-brand"><Img className="logo" fixed={data.file.childImageSharp.fixed} /> Cikal Cendikia Salsabila</h3></Link>
      <nav className="nav nav-header justify-content-center">
        <Link className="nav-link" to="/about" >About</Link>
        <Link className="nav-link" to="/gallery" >Gallery</Link>
        <Link className="nav-link" to="/contacts" >Contacts</Link>
      </nav>
    </header>
  )
}



export default Header
