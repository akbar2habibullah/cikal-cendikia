import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const ContactsPage = ({ data }) => (
  <Layout>
    <SEO title="Contacts" />
    <div className="sosmed mx-auto text-left">
      <i class="fab fa-facebook-square mb-2"></i>
      <h3> {data.datoCmsContact.facebook}</h3>
      <br />
      <i class="fab fa-instagram-square mb-2"></i>
      <h3> {data.datoCmsContact.instagram}</h3>
      <p className="mt-3">
        <span className="highlight title">REKENING</span>
        <br />
        Bank : {data.datoCmsContact.rekeningBank}
        <br />
        No Rek : {data.datoCmsContact.nomorRekening}
        <br />
        A/n : {data.datoCmsContact.rekeningAN}
        <br />
      </p>
    </div>
    <div className="mt-4 mx-auto" style={{ maxWidth: 400 }}>
      {data.datoCmsContact.alamat}
    </div>
  </Layout>
)

export const contactQuery = graphql`
  query contactQuery {
    datoCmsContact {
      alamat
      facebook
      instagram
      nomorRekening
      rekeningAN
      rekeningBank
    }
  }
`

export default ContactsPage
