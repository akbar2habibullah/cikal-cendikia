import React from "react"
import { Card, CardBody, CardImg, CardText } from "reactstrap"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const GalleryPage = ({ data }) => (
  <Layout>
    <SEO title="Gallery" />
    <h4 className="title highlight mt-5 mb-4 mx-auto">Galeri Kami</h4>
    <div className="row justify-content-center">
      {data.allDatoCmsPost.edges.map(post => (
        <Card className="col-12 col-md-5 col-lg-3 m-2">
          <CardBody>
            <CardImg
              className="rounded"
              top
              width="100%"
              src={post.node.image.url}
              alt={post.node.title}
            />
            <CardText className="text-center mt-3">{post.node.title}</CardText>
          </CardBody>
        </Card>
      ))}
    </div>
  </Layout>
)

export const galleryQuery = graphql`
  query galeryQuery {
    allDatoCmsPost {
      edges {
        node {
          title
          image {
            url
          }
        }
      }
    }
  }
`

export default GalleryPage
