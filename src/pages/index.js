import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Home" />
    <h3 className="lead">{data.datoCmsHome.subHeader}</h3>
    <h1 className="cover-heading">"{data.datoCmsHome.header}"</h1>
  </Layout>
)

export const indexQuery = graphql`
  query indexQuery {
    datoCmsHome {
      header
      subHeader
    }
  }
`

export default IndexPage
