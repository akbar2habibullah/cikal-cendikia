import React from "react"
import { Card, CardBody, CardText } from "reactstrap"
import { graphql } from "gatsby"
import Markdown from "react-markdown-it"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="About" />
      <h4 className="title highlight mt-5 mb-4">Tentang Kami</h4>
      <Card className="p-0">
        <CardBody>
          <CardText>
            <Markdown>{data.datoCmsAbout.latarBelakang}</Markdown>
          </CardText>
        </CardBody>
      </Card>
      <h4 className="title highlight mt-5 mb-4">Manhaj, Visi, dan Misi</h4>
      <div className="row justify-content-around">
        <Card className="col-lg-6 mx-3 my-1 p-0">
          <CardBody>
            <CardText>
              <Markdown>{data.datoCmsAbout.manhaj}</Markdown>
            </CardText>
          </CardBody>
        </Card>
        <Card className="col-lg-5 mx-3 my-1 p-0">
          <CardBody>
            <CardText>
              <Markdown>{data.datoCmsAbout.visi}</Markdown>
              <hr />
              <Markdown>{data.datoCmsAbout.misi}</Markdown>
            </CardText>
          </CardBody>
        </Card>
      </div>
      <h4 className="title highlight mt-5 mb-4">Kepengurusan</h4>
      <p>
        Stuktur Yayasan Cikal Cendikia Salsabila terdiri dari Dewan Pembina
        Yayasan, Dewan Pengawas Dan Pengurus Yayasan, berikut adalah stuktur
        Yayasan Cikal Cendikia Salsabila.
      </p>
      <div className="row">
        <div className="col-md-4">
          <p className="highlight">DEWAN PEMBINA</p>
          <Markdown>{data.datoCmsAbout.dewanPembina}</Markdown>
        </div>
        <div className="col-md-4">
          <p className="highlight">DEWAN PENGAWAS</p>
          <Markdown>{data.datoCmsAbout.dewanPengawas}</Markdown>
        </div>
        <div className="col-md-4">
          <p className="highlight">DEWAN PENGURUS</p>
          <Markdown>{data.datoCmsAbout.dewanPengurus}</Markdown>
        </div>
      </div>
      <h4 className="title highlight mt-5 mb-4 text-center">
        Program Unggulan
      </h4>
      <div className="row justify-content-center">
        {data.allDatoCmsProgramUnggulan.edges.map(program => (
          <Card className="col-12 col-md-5 col-lg-3 py-5 m-3">
            <CardBody>
              <CardText>
                <h3 className="title highlight text-center">
                  {program.node.nama}
                </h3>
              </CardText>
            </CardBody>
          </Card>
        ))}
      </div>
    </Layout>
  )
}

export const aboutQuery = graphql`
  query aboutQuery {
    datoCmsAbout {
      misi
      visi
      latarBelakang
      manhaj
      dewanPembina
      dewanPengawas
      dewanPengurus
    }
    allDatoCmsProgramUnggulan {
      edges {
        node {
          nama
        }
      }
    }
  }
`

export default AboutPage
